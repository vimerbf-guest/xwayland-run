'\" t
.\"
.\" Copyright © 2023 Red Hat, Inc
.\"
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License as
.\" published by the Free Software Foundation; either version 2 of the
.\" License, or (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful, but
.\" WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
.\" General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
.\" 02111-1307, USA.

.ds q \N'34'
.TH XWFB-RUN @appmansuffix@
.SH NAME
xwfb-run \- Run X11 clients on a dedicated \fIXwayland\fP server headless.
.SH SYNOPSIS
.B xwfb-run
[option ...] -- COMMAND
.SH DESCRIPTION
.I xwfb-run
is intended as a replacement utility for \fIxvfb-run\fP, using rootful
\fIXwayland\fP running fullscreen on a headless compositor instead of
\fIXvfb\fP.
.SH OPTIONS
.TP 8
.B \-a AUTO_SERVERNUM, --auto-servernum AUTO_SERVERNUM
Unused, kept for backward compatibility only.
.TP 8
.B \-c COMPOSITOR, --compositor COMPOSITOR
Use the compositor class implementation.
.TP 8
.B \-d AUTO_DISPLAY, --auto-display AUTO_DISPLAY
Unused, kept for backward compatibility only.
.TP 8
.B \-e ERROR_FILE, --error-file ERROR_FILE
File used to store xauth and X server errors.
.TP 8
.B \-f AUTH_FILE, --auth-file AUTH_FILE
File used to store auth cookie.
.TP 8
.B \-n SERVER_NUM, --server-num SERVER_NUM
 Xserver number to use.
.TP 8
.B \-l, --listen-tcp
Enable TCP port listening in the X server.
.TP 8
.B \-p XAUTH_PROTO, --xauth-proto XAUTH_PROTO
Xauthority protocol name to use.
.TP 8
.B \-s SERVER_ARGS, --server-args SERVER_ARGS
Arguments to pass to the X server. Xserver arguments must be escaped with
two backslash characters, e.g. \fI-s \\\\-ac\fP.
.TP 8
.B \-w WAIT, --wait WAIT
Delay in seconds to wait for the X server to start before running COMMAND.
.TP 8
.B \-z COMPOSITOR_ARGS, --compositor-args COMPOSITOR_ARGS
Arguments to pass to the Wayland compositor. Arguments intended to the Wayland
compositor must be escaped with two backslash characters, e.g. \fI-z \\\\--renderer -z gl\fP.
.SH FILES
.TP 8
.I wlheadless.conf
.PP
Sets the default compositor to use.
.PP
The file is searched in \fBwlheadless\fP directory relative to the paths
specified in the standard environment variables \fBXDG_DATA_HOME\fP and
\fBXDG_DATA_DIRS\fP.
.PP
.NF
[DEFAULT]
    Compositor = weston
.FI
.SH "SEE ALSO"
\fIwlheadless-run\fP(@appmansuffix@), \fIxwayland-run\fP(@appmansuffix@),
\fIXwayland\fP(@appmansuffix@), \fIXvfb\fP(@appmansuffix@)

