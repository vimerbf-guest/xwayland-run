'\" t
.\"
.\" Copyright © 2023 Red Hat, Inc
.\"
.\" This program is free software; you can redistribute it and/or
.\" modify it under the terms of the GNU General Public License as
.\" published by the Free Software Foundation; either version 2 of the
.\" License, or (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful, but
.\" WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
.\" General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
.\" 02111-1307, USA.

.ds q \N'34'
.TH WLHEADLESS @appmansuffix@
.SH NAME
wlheadless-run \- Run a Wayland client on a dedicated headless Wayland compositor.
.SH SYNOPSIS
.B wlheadless-run
[-c compositor ] [compositor option ...] -- client [client options ...]
.SH DESCRIPTION
.I wlheadless-run
is used to spawn a given Wayland client on a dedicated Wayland compositor
running headless.
.SH OPTIONS
.TP 8
.B \-c COMPOSITOR
Use the compositor class implementation. Currently supported compositor
classes are \fIweston\fP, \fIkwin\fP, \fImutter\fP, \fIgnome-kiosk\fP, \fIcage\fP.
.SH FILES
.TP 8
.I wlheadless.conf
.PP
Sets the default compositor to use.
.PP
The file is searched in \fBwlheadless\fP directory relative to the paths
specified in the standard environment variables \fBXDG_DATA_HOME\fP and
\fBXDG_DATA_DIRS\fP.
.PP
.NF
[DEFAULT]
    Compositor = weston
.FI
.SH "SEE ALSO"
\fIweston\fP(@appmansuffix@),  \fIcage\fP(@appmansuffix@),  \fImutter\fP(@appmansuffix@)
